# Manager Tools

Easy node scripts for recurring managerial tasks that managers have to do and want to solve through code. Like data preperation for Periscope.

Needs latest version of node and Yarn.

Run the first time or when you `git pull` a Yarn install through `yarn install`.

## Run Commands

If you add new tools please add them to package.json under scripts so everything is accessible through `yarn run ***`.

### Periscope Scripts

#### E-Mail to Periscope List

`yarn run mails-to-periscope-list`

Take a copied list of e-mail addresses from Google Sheets, paste it into the script. Outputs a nicely list for Periscope usage.

#### Team structure for periscope

`yarn run glusernames-to-ids`

Creates a new SQL file in the periscope folder named `development_members.sql` which holds the definition of all team members, their gitlab-id, team and section

### Download Team.yml locally

`yarn run download-team-yml`

Downloads the team.yml to the periscope folder.
