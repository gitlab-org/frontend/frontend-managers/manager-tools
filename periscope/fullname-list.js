const fs = require('fs');
const path = require('path');
const inquirer = require('inquirer');

const questions = [
  {
    type: 'editor',
    name: 'maillist',
    message: 'Please paste the list of user ids + names inside your favorite editor',
    validate: text => {
      return true;
    },
  },
];

inquirer.prompt(questions).then(answers => {
  let entries = answers.maillist.split('\n');

  const elements = {};
  entries.forEach(element => {
    const subelements = element.split('\t');
    if (subelements.length === 2 && subelements[0] && subelements[1]) {
      elements[subelements[0]] = subelements[1];
    }
  });

  console.log('OUTPUT : ', elements);

  const outpath = path.resolve(__dirname, 'fullnames.json');
  fs.writeFileSync(outpath, JSON.stringify(elements));
});
