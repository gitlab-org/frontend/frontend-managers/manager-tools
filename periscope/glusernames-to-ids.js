const fs = require('fs');
const path = require('path');

const YAML = require('yaml');
const axios = require('axios');

const teamFile = fs.readFileSync(path.resolve(__dirname, 'team.yml'), 'utf8');
const team = YAML.parse(teamFile);

let glCache = {};
const glCachePath = path.resolve(__dirname, 'glids.json');

if (fs.existsSync(glCachePath)) {
  const glCacheFile = fs.readFileSync(glCachePath, 'utf8');
  glCache = JSON.parse(glCacheFile);
}

const fullnamesList = require('./fullnames.json');

async function getUserIdForUserName(userName) {
  try {
    if (glCache[userName]) {
      return glCache[userName];
    } else {
      const response = await axios.get('https://gitlab.com/api/v4/users?username=' + userName);
      if (response.data) {
        if (response.data.length > 0) {
          glCache[userName] = response.data[0].id;
          return response.data[0].id;
        }
      }
    }
  } catch (err) {
    console.error(err);
  }
}

function findReportsForSlug() {
  const reports = [];
  team.forEach(member => {
    if (member.departments.indexOf('Development Department') > -1) {
      if (member.type === 'person' && member.gitlab) {
        reports.push(member);
      } else if (member.type === 'person' && !member.gitlab) {
        console.warn(`${member.name} has no GitLab ID in team.yml`);
      }
    }
  });
  return reports;
}

async function getFullReportInfosForManagerSlug() {
  const reports = findReportsForSlug();

  for (let ind = 0; ind < reports.length; ind++) {
    let selectedReport = reports[ind];
    selectedReport.gitlabId = await getUserIdForUserName(selectedReport.gitlab);

    selectedReport.section = null;
    selectedReport.team = null;

    selectedReport.isManager =
      selectedReport.role.includes('Manager') || selectedReport.role.includes('Director');

    if (selectedReport.role.includes('Director')) {
      selectedReport.team = 'director';
    }

    //Lets figure out the current level of the person
    selectedReport.level = 'intermediate';
    if (selectedReport.role.includes('Senior')) {
      selectedReport.level = 'senior';
    } else if (selectedReport.role.includes('Staff')) {
      selectedReport.level = 'staff';
    } else if (selectedReport.role.includes('Distinguished')) {
      selectedReport.level = 'distinguished';
    } else if (selectedReport.role.includes('Fellow')) {
      selectedReport.level = 'fellow';
    } else if (selectedReport.role.includes('Senior Engineering Manager')) {
      selectedReport.level = 'senior-manager';
    } else if (selectedReport.role.includes('Engineering Manager')) {
      selectedReport.level = 'manager';
    } else if (selectedReport.role.includes('Director')) {
      selectedReport.level = 'director';
    }

    // Lets figure out the actual team
    selectedReport.departments.forEach(department => {
      if (department.includes('Section')) {
        selectedReport.section = department
          .replace('Section', '')
          .trim()
          .toLowerCase();
      } else if (department.includes('Team') && !selectedReport.team) {
        let teamname = department
          .replace('Team', '')
          .trim()
          .toLowerCase()
          .replace(/:/gi, '_')
          .replace(/ /gi, '_');

        if (teamname.includes('_be') || teamname.includes('_fe')) {
        } else {
          if (selectedReport.departments.includes('Frontend')) {
            teamname += '_fe';
          }
        }

        selectedReport.team = teamname;
      }
    });

    selectedReport.technology = '';
    if (selectedReport.team) {
      if (selectedReport.team.includes('_fe')) {
        selectedReport.technology = 'frontend';
      } else if (selectedReport.team.includes('_be')) {
        selectedReport.technology = 'backend';
      } else if (selectedReport.team == 'gitaly') {
        selectedReport.technology = 'go';
      } else if (selectedReport.team == 'gitter') {
        selectedReport.technology = 'node_fullstack';
      } else if (selectedReport.team == 'expansion' || selectedReport.team == 'acquisition') {
        selectedReport.technology = 'fullstack';
      }
    } else {
      console.warn(`${selectedReport.name} has no team name`);
    }

    selectedReport.matchName = selectedReport.name;
    if (fullnamesList[selectedReport.gitlabId]) {
      selectedReport.matchName = fullnamesList[selectedReport.gitlabId];
    }

    if (!selectedReport.section) {
      console.warn(`${selectedReport.name} has no section name`);
    }
    if (!selectedReport.team && !selectedReport.role.includes('Director')) {
      console.warn(`${selectedReport.name} has no team name`);
    }
  }

  return reports.filter(rep => typeof rep.gitlabId !== 'undefined');
}

async function allReports() {
  let allReports = await getFullReportInfosForManagerSlug();

  allReports.sort((a, b) => (a.team < b.team ? -1 : a.team > b.team ? 1 : 0));

  // Save the Cache for next time
  fs.writeFileSync(glCachePath, JSON.stringify(glCache));

  let outStr = '';

  outStr = `SELECT DISTINCT
    user_id,
    decode(user_id`;

  allReports.forEach(report => {
    outStr += `,\n       ${report.gitlabId}, '${report.gitlab}'`;
  });
  
  // add gitlab-dependency-bot for distribution
  outStr += `,\n       3110502, 'gitlab-dependency-bot' \n       ) as user_name,
    decode(user_id`;

  allReports.forEach(report => {
    outStr += `,\n       ${report.gitlabId}, '${report.name.replace(/'/gi, '')}'`;
  });

  outStr += `\n       ) as full_name,
  decode(user_id`;

  allReports.forEach(report => {
    outStr += `,\n       ${report.gitlabId}, '${report.matchName.replace(/'/gi, '')}'`;
  });

  outStr += `\n       ) as match_name,
    decode(user_id`;

  allReports.forEach(report => {
    outStr += `,\n       ${report.gitlabId}, ${report.isManager}`;
  });

  outStr += `\n       ) as isManager,
    decode(user_id`;

  allReports.forEach(report => {
    outStr += `,\n       ${report.gitlabId}, '${report.level}'`;
  });

  outStr += `\n       ) as level,
    decode(user_id`;

  allReports.forEach(report => {
    outStr += `,\n       ${report.gitlabId}, '${report.technology}'`;
  });

  outStr += `\n       ) as technology,
    decode(user_id`;

  allReports.forEach(report => {
    outStr += `,\n       ${report.gitlabId}, '${report.team}'`;
  });

  // add gitlab-dependency-bot for distribution_be
  outStr += `,\n       3110502, 'distribution_be' \n       ) as team,
      decode(user_id`;

  allReports.forEach(report => {
    outStr += `,\n       ${report.gitlabId}, '${report.section}'`;
  });

  // add gitlab-dependency-bot for enablement
  outStr += `,\n       3110502, 'enablement' \n       ) as section
  FROM analytics.gitlab_dotcom_users_xf
  WHERE 
  user_id in (`;

  allReports.forEach((report, ind) => {
    outStr += `${report.gitlabId}`;
    if (ind + 1 < allReports.length) outStr += ',';
  });

  // add gitlab-dependency-bot for distribution
  outStr += `,3110502)`;

  fs.writeFileSync(path.resolve(__dirname, 'development_members.sql'), outStr, 'utf8');
  console.log('Saved to file development_members.sql');
}

allReports();
