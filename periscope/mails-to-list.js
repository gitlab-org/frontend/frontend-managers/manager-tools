const inquirer = require('inquirer');
const questions = [
  {
    type: 'editor',
    name: 'maillist',
    message: 'Please paste the list of mails inside your favorite editor',
    validate: text => {
      if (text.split('\n').length < 3) {
        return 'Must be at least 3 lines.';
      }

      return true;
    },
  },
];

inquirer.prompt(questions).then(answers => {
  let newArr = answers.maillist
    .split('\n')
    .filter(el => el)
    .map(el => `'${el}'`);

  console.log('\n' + newArr.join(', ') + '\n');
});
