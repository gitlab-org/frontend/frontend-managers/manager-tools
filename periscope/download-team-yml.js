const fs = require('fs');
const path = require('path');
const axios = require('axios');

async function downloadTeamYML() {
  const url = 'https://about.gitlab.com/company/team/team.yml';
  const outpath = path.resolve(__dirname, 'team.yml');
  const writer = fs.createWriteStream(outpath);

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  });

  response.data.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on('finish', resolve);
    writer.on('error', reject);
  });
}

downloadTeamYML();
